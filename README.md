#Path Of Lowest Cost Kata
This is an Android implementation of the Path Of Lowest Cost kata. See below for more details on the kata.

##Build/test
The application uses Gradle.  After cloning, you can run various tasks:
* `./gradlew test` to test
* `./gradlew build` to build

Of course, you can always import it into Android Studio...

##Integration
This project utilizes a separate Java library (PathOfLowestCost-lib).

###Input
The input consists of a sequence of row specifications. Each row is represented by a series of delimited integers on a single line. Note: integers are not restricted to being positive.

###Output
Three lines should be output for each matrix specification. The first line is either “Yes” or “No” to indicate the path made it all the way through the grid. The second line is the total cost. The third line shows the path taken as a sequence of n delimited integers, each representing the rows traversed in turn. If there is more than one path of least cost, only one path need be shown in the solution.